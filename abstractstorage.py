import abc
from ataclasses import Memo


class AbstractStorage(abc.ABC):
    @abc.abstractmethod
    def add(self, memo: Memo):
        pass

    @abc.abstractmethod
    def rm(self, memo_id: int):
        pass

    @abc.abstractmethod
    def list(self):
        pass

    @abc.abstractmethod
    def get(self, memo_id: int):
        pass