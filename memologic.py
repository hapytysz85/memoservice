from ramstorage import RamStorage
from ataclasses import Memo


class MemoLogic:
    def __init__(self):
        self.__storage = RamStorage()

    def list(self) -> dict:
        return self.__storage.list()

    def add(self, memo: Memo):
        return self.__storage.add(memo)

    def rm(self, id):
            self.__storage.rm(id)

    def get(self, memo_id: int):
        return self.__storage.get(memo_id)