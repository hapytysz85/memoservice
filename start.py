from flask import Flask, request
from memologic import MemoLogic
from ataclasses import Memo
from flask import abort
import json

app = Flask(__name__)
logic = MemoLogic()


@app.errorhandler(Exception)
def handle_bad_request(e):
    return json.loads(json.dumps({'error':str(e)})), 404


@app.route('/memo', methods=['GET'])
def get_all():
    # return json.loads(json.dumps({k: json.loads(v.to_json()) for k, v in logic.list().items()}))
    return json.loads(json.dumps(logic.list(), default=lambda x: x.__dict__))


@app.route('/memo/<int:memo_id>', methods=['GET'])
def get_by_id(memo_id):
    return json.loads(json.dumps(logic.get(memo_id).__dict__))


@app.route('/memo', methods=['POST'])
def add():
    json_data = json.dumps(request.json)
    memo = Memo(**json.loads(json_data))
    logic.add(memo)
    return json.loads(json.dumps(memo.__dict__, indent=2, ensure_ascii=True))


if __name__ == '__main__':
    app.run(debug=True)