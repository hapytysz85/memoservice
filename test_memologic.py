from memologic import MemoLogic
from ataclasses import Memo


def test_add_memo():
    logic = MemoLogic()
    logic.add(Memo("test1"))
    assert len(logic.list()) == 1


def test_rm_memo():
    logic = MemoLogic()
    logic.add(Memo("test1"))
    logic.rm(0)
    assert len(logic.list()) == 0
