from abstractstorage import AbstractStorage
from ataclasses import Memo


class RamStorage(AbstractStorage):
    def __init__(self):
        self.__data = {}
        self.__last_id = 0

    def get(self, memo_id: int):
        try:
            return self.__data[memo_id]
        except:
            raise Exception("No memo_id")

    def add(self, memo: Memo):
        self.__data[self.__last_id] = memo
        memo.id = self.__last_id
        self.__last_id += 1
        return memo

    def rm(self, memo_id: int):
        try:
            del self.__data[memo_id]
        except:
            raise Exception("Niepoprawne id")

    def list(self):
        return self.__data
