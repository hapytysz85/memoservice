from dataclasses import dataclass
from dataclasses_json import dataclass_json

@dataclass_json
@dataclass(init=False)
class Memo:
    name: str
    id: int

    def __init__(self, name: str):
        self.name = name
        self.id = -1

